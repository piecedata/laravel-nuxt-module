export default (options) => ({
  namespaced: true,

  state: {
    data: {},
  },

  getters: {},

  actions: {
    set({ commit }, data) {
      commit('SET', data);
    },
  },

  mutations: {
    SET(state, data) {
      state.data = data;
    },
  },
})
