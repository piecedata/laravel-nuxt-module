import Middleware from '../../middleware';

const options = JSON.parse(`<%= JSON.stringify(options) %>`);
const { namespace } = options;

let i = 0;

Middleware[namespace] = async ctx => {
  const { isDev, app, store, route, error, $axios, redirect, next } = ctx;

  if (i < 2) {
    i++;
  }

  let data;

  // check initial state and only for first request
  if (i === 1 && window.__INITIAL_STATE__) {
    data = window.__INITIAL_STATE__;
  }

  // fetch data
  try {
    if (!data) {
      $axios.defaults.headers.common['X-Inertia'] = true;

      const response = await $axios.get(route.fullPath);

      let redirectLocation = response.headers['x-inertia-location'];

      if (redirectLocation) {
        if (isDev && redirectLocation.indexOf(process.env.API_URL) === 0) {
          redirectLocation = String(redirectLocation)
            .replace(process.env.API_URL, '');
        } else if (redirectLocation.indexOf(window.location.origin) === 0) {
          redirectLocation = String(redirectLocation)
            .replace(window.location.origin, '');
        } else {
          next(false);
          window.location.href = redirectLocation;
          return;
        }

        return redirect(redirectLocation);
      }

      data = response.data;
    }

    store.dispatch(`${namespace}/set`, data);

    // set csrfToken
    $axios.defaults.headers.common['X-CSRF-TOKEN'] = data.csrfToken;

    // update document title but...
    app.head.title = data.title;
    document.title = data.title;
  } catch ({ response }) {
    // todo: if status 409 and x-inertia-location hard reload
    if (response) {
      const { status: statusCode, statusText: message } = response;

      return error({
        statusCode,
        message,
      });
    }

    return error('Error');
  }
};
