import { resolve, join } from 'path';
import { readdirSync } from 'fs';

// const fsExtra = require('fs-extra');

export default function LaravelNuxtModule(moduleOptions) {
  const options = {
    ...moduleOptions,
    ...this.options.laravelNuxt,
  };

  // expose the namespace / set a default
  if (!options.namespace) {
    options.namespace = 'laravelNuxt';
  }
  const { namespace } = options;

  this.options.appTemplatePath = resolve(__dirname, 'app.template.html');

  if (!this.options.meta) {
    this.options.meta = {};
  }

  this.options.meta = Object.assign({
    name: false,
    description: false,
    author: false,

    ogDescription: false,
    ogHost: false,
    ogImage: false,
    ogSiteName: false,
    ogTitle: false,
    ogType: false,
    ogUrl: false,

    twitterCard: false,
    twitterSite: false,
    twitterCreator: false,
  }, this.options.meta);

  this.options.loadingIndicator = false;

  // set laravel public dir
  this.options.generate.dir = join(this.options.srcDir, '../', 'public');
  this.options.generate.fallback = false;

  // all meta tags are backend handled
  // this.nuxt.hook('generate:before', () => {
  //   this.options.head.meta = [];
  // });

  const pluginsToSync = [
    'store/index.js',
    'middleware/index.js',
  ];

  for (const pathString of pluginsToSync) {
    this.addPlugin({
      src: resolve(__dirname, pathString),
      fileName: join(namespace, pathString),
      ssr: false,
      options,
    });
  }

  // sync all of the files and folders to revelant places in the nuxt build dir (.nuxt/)
  const foldersToSync = ['store/modules'];
  for (const pathString of foldersToSync) {
    const path = resolve(__dirname, pathString);
    for (const file of readdirSync(path)) {
      this.addTemplate({
        src: resolve(path, file),
        fileName: join(namespace, pathString, file),
        ssr: false,
        options,
      });
    }
  }

  // push main middleware
  if (!this.options.router.middleware) {
    this.options.router.middleware = [];
  } else if (typeof this.options.router.middleware === 'string'
    && this.options.router.middleware.length) {
    this.options.router.middleware = [this.options.router.middleware];
  }

  if (Array.isArray(this.options.router.middleware)) {
    this.options.router.middleware = [namespace, ...this.options.router.middleware];
  }

  // proxy
  this.options.proxy = process.env.NODE_ENV === 'development' ? {
    '/api': {
      target: process.env.API_URL,
      pathRewrite: {
        '^/api/': '/',
      },
    },
  } : null;

  if (!this.options.axios) {
    this.optoins.axios = {};
  }

  this.options.axios = {
    proxy: process.env.NODE_ENV === 'development',
    prefix: process.env.NODE_ENV === 'development' ? '/api' : '/',
    ...this.options.axios,
  };

  this.options.build.transpile.push('laravel-nuxt-module');

  // generate index only
  this.nuxt.hook('generate:extendRoutes', (routes) => {
    routes.splice(0, routes.length, ...routes.filter(page => ['/'].includes(page.route)));
  });

  // create app.blade.php
  this.nuxt.hook('generate:page', (page) => {
    if (page.route !== '/') {
      return;
    }

    page.path = '../resources/views/app.blade.php';

    page.html = page.html.replace(/<!--blade::(.*)-->/g, '$1')
      .replace(/(<title.*?>).*?(<\/title>)/g, '$1{{ $title }}$2')
      .replace(/(<html.*lang=").*?(".*>)/g, '$1{{ str_replace(\'_\', \'-\', app()->getLocale()) }}$2');
  });
};

module.exports.meta = require('./package.json');
